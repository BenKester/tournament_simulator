# teams.csv: teams should be and ordered listed in order of seed. first one conference, then the other.
# Games are defined as (home v away):
# 1. #3 v #6
# 2. #9 v #12
# 3. #4 v #5
# 4. #10 v #11
# 5. #1 v max(1, 3)
# 6. #7 v max(2, 4)
# 7. #2 v min(1, 3)
# 8. #8 v min(2, 4)
# 9. 5 v 7
# 10. 6 v 8
# 11 9 v 10

import simulator as sim

def hfa(a, b):
    if b.conference() != a.conference():
        return 0
    elif b.seed() > a.seed():
        return 1
    else:
        return -1
def getteams(results, teams, i):
    if i == 0:
        return teams[2], teams[5]
    elif i == 1:
        return teams[8], teams[11]
    elif i == 2:
        return teams[3], teams[4]
    elif i == 3:
        return teams[9], teams[10]
    elif i == 4:
        return teams[0], results[0].winner if results[0].winner.seed() > results[2].winner.seed() else results[2].winner
    elif i == 5:
        return teams[6], results[1].winner if results[1].winner.seed() > results[3].winner.seed() else results[3].winner
    elif i == 6:
        return teams[1], results[2].winner if results[0].winner.seed() > results[2].winner.seed() else results[0].winner
    elif i == 7:
        return teams[7], results[3].winner if results[1].winner.seed() > results[3].winner.seed() else results[1].winner
    elif i == 8:
        return (results[4].winner, results[6].winner) if results[4].winner.seed() < results[6].winner.seed() else (results[6].winner, results[4].winner)
    elif i == 9:
        return (results[5].winner, results[7].winner) if results[5].winner.seed() < results[7].winner.seed() else (results[7].winner, results[5].winner)
    elif i == 10:
        return results[8].winner, results[9].winner
def points(game, result):
    if game < 4:
        ret = 1
    elif game < 8:
        ret = 2
    elif game < 10:
        ret = 3
    else:
        ret = 5
    return ret + (((ret + 1) // 2) if game < 10 and hfa(result.winner, result.loser) == -1 else 0)

games, results = sim.loadresults('results.txt')
players = sim.loadplayers('picks.csv')
teams, teamsdic = sim.loadteams('teams.csv', hfa)

sim.calcscores(players, teams, teamsdic, games, results, getteams, points, False)
sim.printresults(players)
sim.simscores(players, teams, teamsdic, games, results, getteams, points, 100000, False)
sim.printresults(players)
