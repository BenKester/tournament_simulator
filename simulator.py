# todo:
# rename repo
# set up ncaa pool
# write articles

import random
import csv
import math

class Team:
    def __init__(self, name, elo, order, hfa):
        self.name = name
        self.elo = int(elo)
        self.order = int(order)
        self.hfa = hfa
    def conference(self):
        return (self.order + 1) // 6
    def seed(self):
        return (self.order - 1) % 6 + 1
    def winprob(self, other):
        hfa = self.hfa(self, other) * 65
        return 1 / (pow(10,-(self.elo - other.elo + hfa)/400) + 1)
    def __repr__(self):
        return self.name

class Player:
    def __init__(self, name):
        self.name = name
        self.scores = []
        self.gameswon = 0
        self.preds = dict()
        self.winp = 0
        self.probs = []
        self.totalprob = 1
    def avgscore(self):
        if len(self.scores) == 0: return 0
        return sum(self.scores) / len(self.scores)
    def expscore(self):
        return sum([a*b/self.totalprob for a,b in zip(self.scores,self.probs)])
    def minscore(self):
        return min(self.scores)
    def header():
        return 'name, games won, win prob, exp score, avg score, cur score'
    def __repr__(self):
        return '{} {} {} {} {} {}'.format(self.name, self.gameswon, self.winp/self.totalprob, self.expscore(), self.avgscore(), self.minscore())

class Result:
    def __init__(self, winner, loser):
        self.winner = winner
        self.loser = loser

def nbit(a, n):
    return not not(a & (1 << n));
def addresult(lst, teams, awon):
      win = teams[0] if awon else teams[1]
      los = teams[1] if awon else teams[0]
      lst.append(Result(win, los))
      return win.winprob(los)
def loadteams(file, hfa):
    with open (file, 'r') as f:
        tm = [Team(*x, hfa) for x in list(csv.reader(f))]
        td = dict()
    for t in tm:
        td[t.name] = t
        return tm, td
def loadplayers(file):
    with open (file, 'r') as f:
        first = 1
        for row in csv.reader(f):
            if first:
                first = 0
                firstrow = row
                ret = dict()
                for n in row:
                    ret[n] = Player(n)
            else:
                for i in range(0, len(row)):
                    ret[firstrow[i]].preds[row[i]] = ret[firstrow[i]].preds.get(row[i], 0) + 1
    return ret
def loadresults(file):
    with open (file, 'r') as f:
        s = f.read()
        games = len(s)
        results = int(s, 2) if games > 0 else 0
    return games, results
def scoreplayer(result, player):
    score = 0
    for t in result:
        for i in range(0, len(result[t])):
            if player.preds.get(t.name, 0) > i:
                score += result[t][i]
    return score
def process_scenario(itr, players, getpoints, prob, printresults):
    results = dict()
    for j in range(0, len(itr)):
        if not itr[j].winner in results:
            results[itr[j].winner] = []
        results[itr[j].winner].append(getpoints(j, itr[j]))
    maxscore = 0
    for p in players.values():
        p.probs.append(prob)
        s = scoreplayer(results, p)
        p.scores.append(s)
        if s > maxscore:
            maxscore = s
    winners = []
    for p in players.values():
        if p.scores[-1] == maxscore:
            winners.append(p)
    for p in winners:
        p.gameswon += 1 / len(winners)
        p.winp += prob / len(winners)
        if printresults: print(p.name,end=' ')
def calcscores(players, teams, teamsdic, games, results, getteams, getpoints, printresults=False):
    n = pow(2, len(teams) - 1)
    totalprob = 0
    for i in range(results, n, pow(2, games)):
        prob = 1
        itr = []
        for j in range(0, len(teams) - 1):
            prob *= addresult(itr, getteams(itr, teams, j), nbit(i, j))
        if printresults: print(prob, end=' ')
        totalprob += prob
        process_scenario(itr, players, getpoints, prob, printresults)
    for p in players.values():
        p.totalprob = totalprob
    if printresults: print(results)
def simscores(players, teams, teamsdic, games, results, getteams, getpoints, iterations, seed=None, printresults=False):
    random.seed(seed)
    for i in range(0, iterations):
        itr = []
        for j in range(0, len(teams) - 1):
            t = getteams(itr, teams, j)
            addresult(itr, t, t[0].winprob(t[1]) > random.random() if j >= games else nbit(results, j))
        process_scenario(itr, players, getpoints, 1, printresults)
    for p in players.values():
        p.totalprob = iterations
    if printresults: print(results)
def printresults(players):
    print(Player.header())
    for p in players.values():
        print(p)
